-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
-- -----------------------------------------------------
-- Schema student_progress
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema student_progress
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `student_progress` DEFAULT CHARACTER SET utf8mb4 ;
USE `student_progress` ;

-- -----------------------------------------------------
-- Table `student_progress`.`photograph`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `student_progress`.`photograph` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `photograph_url` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `student_progress`.`subject`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `student_progress`.`subject` (
  `id` INT NOT NULL,
  `subject_name` VARCHAR(45) NULL,
  `semester` INT NULL,
  `lecturer_id` INT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `student_progress`.`group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `student_progress`.`group` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `specialty` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `student_progress`.`address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `student_progress`.`address` (
  `id` INT NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  `address` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `student_progress`.`lecturer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `student_progress`.`lecturer` (
  `id` INT NOT NULL,
  `name_surname` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `student_progress`.`lecturer_teaches_subject`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `student_progress`.`lecturer_teaches_subject` (
  `subject_id` INT NOT NULL,
  `lecturer_id` INT NOT NULL,
  PRIMARY KEY (`subject_id`, `lecturer_id`),
  INDEX `fk_subject_has_lecturer_lecturer1_idx` (`lecturer_id` ASC),
  INDEX `fk_subject_has_lecturer_subject_idx` (`subject_id` ASC),
  CONSTRAINT `fk_subject_has_lecturer_subject`
    FOREIGN KEY (`subject_id`)
    REFERENCES `student_progress`.`subject` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_subject_has_lecturer_lecturer1`
    FOREIGN KEY (`lecturer_id`)
    REFERENCES `student_progress`.`lecturer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `student_progress`.`module_result`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `student_progress`.`module_result` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `subject_id` INT NOT NULL,
  PRIMARY KEY (`id`, `subject_id`),
  INDEX `fk_module_result_subject1_idx` (`subject_id` ASC),
  CONSTRAINT `fk_module_result_subject1`
    FOREIGN KEY (`subject_id`)
    REFERENCES `student_progress`.`subject` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `student_progress`.`scholarship`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `student_progress`.`scholarship` (
  `id` INT NOT NULL,
  `scholarship_amount` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `student_progress`.`rating`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `student_progress`.`rating` (
  `id` INT NOT NULL,
  `place` INT NULL,
  `rating_mark` INT NULL,
  `scholarship_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_rating_scholarship1_idx` (`scholarship_id` ASC),
  CONSTRAINT `fk_rating_scholarship1`
    FOREIGN KEY (`scholarship_id`)
    REFERENCES `student_progress`.`scholarship` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `student_progress` ;

-- -----------------------------------------------------
-- Table `student_progress`.`student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `student_progress`.`student` (
  `surname_name` VARCHAR(255) NULL DEFAULT NULL,
  `autobiography` VARCHAR(255) NULL DEFAULT NULL,
  `birth_year` DATE NULL DEFAULT NULL,
  `admission_year` DATE NULL DEFAULT NULL,
  `address_id` INT(11) NULL DEFAULT NULL,
  `photograph_id` INT NOT NULL,
  `group_id` INT NOT NULL,
  `address_id1` INT NOT NULL,
  `rating_id` INT NOT NULL,
  PRIMARY KEY (`photograph_id`, `group_id`),
  INDEX `fk_student_group1_idx` (`group_id` ASC),
  INDEX `fk_student_address1_idx` (`address_id1` ASC),
  INDEX `fk_student_rating1_idx` (`rating_id` ASC),
  CONSTRAINT `fk_student_photograph`
    FOREIGN KEY (`photograph_id`)
    REFERENCES `student_progress`.`photograph` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_group1`
    FOREIGN KEY (`group_id`)
    REFERENCES `student_progress`.`group` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_address1`
    FOREIGN KEY (`address_id1`)
    REFERENCES `student_progress`.`address` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_rating1`
    FOREIGN KEY (`rating_id`)
    REFERENCES `student_progress`.`rating` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `student_progress`.`student_has_subject`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `student_progress`.`student_has_subject` (
  `student_photograph_id` INT NOT NULL,
  `subject_id` INT NOT NULL,
  PRIMARY KEY (`student_photograph_id`, `subject_id`),
  INDEX `fk_student_has_subject_subject1_idx` (`subject_id` ASC),
  INDEX `fk_student_has_subject_student1_idx` (`student_photograph_id` ASC),
  CONSTRAINT `fk_student_has_subject_student1`
    FOREIGN KEY (`student_photograph_id`)
    REFERENCES `student_progress`.`student` (`photograph_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_has_subject_subject1`
    FOREIGN KEY (`subject_id`)
    REFERENCES `student_progress`.`subject` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `student_progress`.`student_has_module_result`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `student_progress`.`student_has_module_result` (
  `student_photograph_id` INT NOT NULL,
  `student_group_id` INT NOT NULL,
  `module_result_id` INT NOT NULL,
  PRIMARY KEY (`student_photograph_id`, `student_group_id`, `module_result_id`),
  INDEX `fk_student_has_module_result_module_result1_idx` (`module_result_id` ASC),
  INDEX `fk_student_has_module_result_student1_idx` (`student_photograph_id` ASC, `student_group_id` ASC),
  CONSTRAINT `fk_student_has_module_result_student1`
    FOREIGN KEY (`student_photograph_id` , `student_group_id`)
    REFERENCES `student_progress`.`student` (`photograph_id` , `group_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_has_module_result_module_result1`
    FOREIGN KEY (`module_result_id`)
    REFERENCES `student_progress`.`module_result` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
