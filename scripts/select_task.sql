select distinct maker from product
where type = "pc" and type in (select type from product
where type != "notebook");
select distinct maker from product
where type != all (select type
from product where type = "laptop" or type = "printer");
select distinct maker from product
where type != any (select type
from product where type = "laptop" or type = "printer")
and type = "pc";
select distinct pr.maker from
product as pr
join product as pt
where pr.type in ("laptop")
and pt.type = "pc";
select pt.maker from
(select distinct pr.maker, pr.type 
from product as pr
where pr.type != all
(select distinct type
from product
where type = "printer")) pt
group by pt.maker
having count(pt.maker) > 1;
select avg(laptop.price) as "avg price"
 from laptop;